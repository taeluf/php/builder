<?php

namespace Tlf\Builder;

/**
 * Determines latest version from list of tags & determine next version from input + bump rules
 *
 * For git-related version management, use GitVersionBumper instead.
 *
 */
class VersionBumper {

    /**
     * Get the index that should be bumped, depending on the commit message and bump rules.
     *
     * @param $commit_message string a commit message with or without a 'prefix:'. 
     * @param $bump_rules array<int index_in_version_string, array string_prefixes> string prefixes are literal, except a single asterisk (`*`) will always bump that version number, and should only be present if you want a default case of bumping the last index
     *
     * @return int index to increase by 1, or -1 if there should not be a version increase.
     */
    static public function get_index_to_bump(string $commit_message, array $bump_rules): int{

        foreach ($bump_rules as $index_to_bump => $prefixes_to_cause_bump){
            foreach ($prefixes_to_cause_bump as $prefix){
                if ($prefix=='*')return $index_to_bump;
                $len = strlen($prefix);
                if (substr($commit_message,0,$len+1) == "$prefix:"){
                    return $index_to_bump;
                }
            }
        }

        return -1;
    }


    /**
     * Bump a version string at the index, returning a string with all zeros after the bumped index, and any zeros filled in if the input version is not long enough.
     *
     * @example 3.0.7, index 1, yields 3.1.0
     * @example 1.0, index 3, yields 1.0.0.1
     *
     * @param $version string like "3.0.7" or "0.18.6.4"
     * @param $index_to_bump int zero-based index. Accepts out-of-range indexes.
     *
     * @return
     *
     */
    static public function get_bumped_version(string $version, int $index_to_bump): string {

        $parts = explode(".", $version);
        $original_len = count($parts);
        $zeros_to_add = 1 + $index_to_bump - $original_len;
        while ($zeros_to_add-- > 0){
            $parts[] = 0;
        }

        $new_num = ((int)$parts[$index_to_bump] ?? 0) + 1;
        $parts[$index_to_bump] = $new_num;
        $len = count($parts);
        while (++$index_to_bump < $len){
            $parts[$index_to_bump] = 0;
        }

        $new_version = implode(".", $parts);

        return $new_version;
    }

    /**
     * Get the latest version from the version_list having the given version prefix.
     *
     * @param $version_list string one version per line, separated by \n (no *\r*) as returned from `git tag --list`
     * @param $version_prefix string like "1.0" or "0.8"
     *
     * @return string version like 1.1 or 0.9
     */
    static public function get_latest_version(string $versions_list, string $version_prefix=''): string {

        $version_strings = array_map('trim',explode("\n", trim($versions_list)));

        $prefix_len = strlen($version_prefix);

        $maxes = [];
        $max_version_number = null;

        foreach ($version_strings as $index => $version_string){
            if (substr($version_string, 0, $prefix_len) != $version_prefix);

            //echo "\n   Version string: '$version_string'";
            
            $relevant_version_string = substr($version_string, $prefix_len);
            if ($relevant_version_string[0] == '.')$relevant_version_string = substr($relevant_version_string,1);
            //echo "\n   Relevant version string: '".$relevant_version_string."'";
            $parts = explode(".", $relevant_version_string);


            //print_r($parts);

            foreach ($parts as $part_index => $part_number){
                $existing_max = $maxes[$part_index] ?? -1;
                if ($part_number < $existing_max)break;
                else if ($part_number > $existing_max){
                    $maxes[$part_index] = $part_number;                   
                    $i = $part_index;
                    while (isset($maxes[++$i])){
                        $maxes[$i] = 0;
                    }
                    $max_version_number = $version_string;
                } else {
                    // continue is here just to be explicit
                    continue;
                }
            }
        }

        if ($max_version_number == null) {
            throw new \Exception("Could not determine max version number.");
        }

        return $max_version_number;
    }



    /**
     * Get a version that is at least $min_length places long.
     *
     * Does not shorten version strings.
     *
     * @param $version string like "1.0"
     * @param $min_length int like 3, yielding "1.0.0"
     *
     * @return string like "1.0" or "1.0.0.0" if min_length is 4.
     */
    static public function get_version_of_minlength(string $version, int $min_length): string {
        
        $parts = explode(".", $version);
        $c = count($parts);
        $length_diff = $min_length - $c;
        if ($c == 1 && $parts[0] == '')$version = '0';
        while ($length_diff-- > 0){
            $version .= '.0';
        }

        return $version;

    }

}
