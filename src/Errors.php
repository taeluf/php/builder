<?php

namespace Tlf\Builder;

/**
 * Simple class to hold error messages
 */
class Errors {

    public const define_bin_script = "\n\nYou must define a 'bin' script in your 'create_phar' pipeline options. Path must be relative to project root, such as bin/my-script.php\n\n";

    public const config_file_missing = "\n\nA config file does not exist at config/tlfbuild.json. Create one.\n\n";

}
