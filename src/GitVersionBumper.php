<?php

namespace Tlf\Builder;

/**
 * Determines latest version from list of tags & determine next version from input + bump rules
 *
 */
class GitVersionBumper {

    /**
     * Get the new version for the git repo for the current working directory.
     * YOU MUST: `git fetch` manually before calling this for up-to-date answers.
     *
     *
     * @param $bump_rules array<int index_in_version_string, array string_prefixes> string prefixes are literal, except a single asterisk (`*`) will always bump that version number, and should only be present if you want a default case of bumping the last index
     *
     * @return string version like 0.8.0.1 from 0.8.0.0
     */
    static public function get_new_version_from_git(array $bump_rules): string {
        ob_start();
        system("git log -1 --pretty=%B");
        $last_commit = ob_get_clean();

        ob_start();
        exec("git rev-parse --abbrev-ref HEAD");
        $branch_version = ob_get_clean();
        $numeric_version = preg_replace('/[^0-9\.]/','', $branch_version);

        ob_start();
        system("git tags --list");
        $tags_list = ob_get_clean();

        return VersionBumper::bump($numeric_version, $highest_version, $bump_rules, 
        return VersionBumper::bump($numeric_version, $tags_list, $bump_rules, $last_commit);

        // TODO: Create one function that takes $tags_list, $numeric_version, $last_commit, and $bump_rules all together
        // It should go in the main version bumper
        // It should be unit tested
        // And this method should just call it and return

        $latest_tag = VersionBumper::get_latest_version($tags_list, $numeric_version);
        $index_to_bump = VersionBumper::get_index_to_bump($last_commit, $bump_rules);

        $new_version = VersionBumper::get_bumped_version($latest_tag, $index_to_bump);

        return VersionBumper::get_version_of_minlength($version, count($bump_rules));
    }

}
