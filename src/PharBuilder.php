<?php

namespace Tlf\Builder;

/**
 * A simple class that builds phars.
 */
class PharBuilder {


    /**
     * @param $phar Phar a phar instance to build with.
     */
    public function __construct(public \Phar $phar){
        
    }

    /**
     * Add all .php files in `$dir` to the phar.
     *
     * @param $dir string directory to scan
     * @param $rel_prefix string relative path prefix to use within the phar archive
     */
    public function add_project_dir(string $dir, string $rel_prefix = ''){
        echo "\nSearch dir $dir";
        $dirator = new \RecursiveDirectoryIterator($dir);
        $iterator = new \RecursiveIteratorIterator($dirator);

        //$this->phar->startBuffering();
        $len = strlen($dir);
        foreach ($iterator as $path){
            $basename = basename($path);
            $ext = strtolower(pathinfo($basename, PATHINFO_EXTENSION));
            $rel = substr($path, $len);
            if ($rel[0]=='/')$rel = substr($rel,1);
            $root = substr($rel, 0, strpos($rel, "/"));

            if ($ext != 'php'
                //&& 
                //(!is_file($path) || !is_executable($path))
            )continue;

            $this->phar->addFile($path, $rel_prefix.$rel);
        }

        //$this->phar->stopBuffering();

    }

    //public function make_executable(){
        //$phar_path = $this->phar->getPath();
        ////$this->phar->convertToExecutable(\Phar::PHAR, \PHAR::NONE, 'phar');
        ////file_put_contents($phar_path, "#!/usr/bin/env php\n".file_get_contents($phar_path));
        //system("chmod ug+x \"$phar_path\"");
    //}

    public function output_phar(){

    }
}
