<?php

namespace Tlf\Builder;

/**
 * Provides a build pipeline to generate PHARs.
 * May provide additional build options in the future.
 */
class ProjectBuilder {

    /**
     * array of methods on this class to call
     * methods must return TRUE or build will stop. return FALSE on error.
     */
    private array $pipeline = [
        'check_composer_json'=>[],
        'check_git_ignore'=>[],
        'create_composer_phar'=>[],

    ];

    private string $phar_name = "project.phar";

    /** relative path to build dir */
    private string $build_dir = "build/";

    /**
     * Load a config for a project build.
     * @param $file_path string absolute file path. File must exist.
     */
    public function load_config(string $file_path){
        $content = file_get_contents($file_path);
        $settings = json_decode($content, true);
        $this->pipeline = $settings['pipeline'] ?? $this->pipeline;
        $this->phar_name = $settings['phar_name'] ?? $this->phar_name;
        $this->build_dir = $settings['build_dir'] ?? $this->build_dir;
    }


    public function build(){
        foreach ($this->pipeline as $build_step => $settings){
            echo "\nBuild: $build_step\n";
            $continue_build = $this->$build_step($settings);
            if (!$continue_build){
                echo "\n\n    Build stopped by build step '$build_step'\n\n";
                break;
            }
        }
    }

    /**
     * Check that composer.json exists and stop build if it does not.
     * @return bool true if exists, false if not
     */
    public function check_composer_json(): bool {
        $file = getcwd().'/composer.json';
        if (!file_exists($file)){
            echo "\n\n   composer.json does not exist. run `composer init`";
            echo   "\n     See https://getcomposer.org/ to install composer\n\n";
            return false;
        }
        return true;
    }

    /**
     * Check if `.gitignore` lists `build/` & offer to add it to the file
     *
     * @return bool true always 
     */
    public function check_git_ignore(): bool {
        $file = getcwd().'/.gitignore';
        if (!is_file($file)){
            // offer to create file
            echo "\n";
            $answer = readline(".gitignore file not found. Create? (y/n)");
            if ($answer == "y" || $answer == "Y"){
                touch($file);
                echo "\n  '.gitignore' file created\n";
            }
        }

        $content = file_get_contents($file);
        $lines = explode("\n", $content);
        if (array_search("build/", $lines) === false){
            // offer to add build/ to the gitignore
            echo "\n.gitignore:\n  'build/' not ignored\n";

            $answer = readline("Append 'build/' to .gitignore? (y/n)");
            if ($answer == "y" || $answer == "Y"){
                $fh = fopen($file,'a');
                fwrite($fh, "\nbuild/");
                fclose($fh);
                echo "\n  'build/' added to gitignore\n";
            }
        }

        return true;
    }

    /**
     * Create a phar using clue/phar-composer. 
     */
    public function create_composer_phar(): bool {
        $path = __DIR__.'/../bin/build-composer-phar';
        $cmd = "\"$path\"";
        ob_start();
        system(file_get_contents($path));
        //system($cmd);
        $output = ob_get_clean();

        return true;
    }

    /**
     * Create a phar using built-in PharBuilder, which does not automatically build composer dependencies into the phar
     */
    public function create_phar(array $options): bool {
        require_once(__DIR__ .'/../src/PharBuilder.php');
        $phar_rel_path = $this->build_dir.'/'.$this->phar_name;
        $phar_path = getcwd().'/'.$phar_rel_path;
        if (is_file($phar_path)){
            echo "Delete ".$this->build_dir.'/'.$this->phar_name;
            unlink($phar_path);
        }


        echo "\n\nCreate PHAR at ".$phar_rel_path."\n";

        $phar = new \Phar($phar_path);
        if (!isset($options['bin'])){
            echo Errors::define_bin_script; 
            exit;
        }

        $bin_script = $options['bin'];
        $phar->addFile(getcwd().'/'.$bin_script, $bin_script);

        $phar->setDefaultStub($bin_script);
        //$phar->addFile(__DIR__.'/build-phar', 'bin/build-phar');
        $pharbuilder = new \Tlf\Builder\PharBuilder($phar);

        foreach ($options['dirs'] ?? ['src/'] as $dir){
            $pharbuilder->add_project_dir(getcwd().'/'.$dir, $dir);
        }
        

        //echo "\n";
        //$ug_x= readline("Make 'build/".$this->phar_name."' executable?");
        //if ($ug_x=="y" || $ug_x == "Y"){
            //$pharbuilder->make_executable(); 
        //}

        return true;
    }
}
