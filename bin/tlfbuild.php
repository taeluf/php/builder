#!/usr/bin/env php
<?php

use \Tlf\Builder\Errors;

echo "\n";

$args = $argv;

$command = $args[1] ?? "--no-command--";

require_once(__DIR__.'/../src/Errors.php');

switch ($command){

    case "help":
    case "--help":
    case "-h":
        echo <<<HELP
            tlfbuild [command] - Run build & release steps

                help, -h, --help - show this help menu
                build - Build your project
                release - Bump your project version & create a new, versioned git tag containing your built phar
                config - Display your configuration
                self-test - Run tlfbuild's own tests.
        HELP;
        break;
    case "self-test":
        $_total_pass_count = 0;
        $_total_fail_count = 0;
        foreach (scandir($dir=__DIR__.'/../test/') as $f){
            if (substr($f,-4)!='.php')continue;
            $pass_count = 0;
            $fail_count = 0;

            $filename = basename(substr($f,0,-4));
            echo "\n\n--------Run Test File $filename--------";
            require($dir.'/'.$f);

            $_total_pass_count += $pass_count;
            $_total_fail_count += $fail_count;
        }

        echo "\n\nTotal Tests Passed: $_total_pass_count";
        echo "\nTotal Tests Failed: $_total_fail_count";
        break;
    case "build":
        require_once(__DIR__ .'/../src/ProjectBuilder.php');
        $builder = new \Tlf\Builder\ProjectBuilder();
        
        $conf = getcwd().'/config/tlfbuild.json';
        if (file_exists($conf)){
            $builder->load_config($conf);
        }
        
        $builder->build();
        break;
    case "release":
        echo "\n\nRELEASE UNDER DEVELOPMENT\n\n";
        exit;
        // TODO
        // My initial plan was copy+paste Project Builder, then just create different pipeline stuffs ...
        // But I could make releasing more opinionated or simpler or something
        // I don't know
        //
        //
        require_once(__DIR__ .'/../src/ProjectReleaser.php');
        $builder = new \Tlf\Builder\ProjectReleaser();
        
        $conf = getcwd().'/config/tlfbuild.json';
        if (file_exists($conf)){
            $builder->load_config($conf);
        }
        
        // TODO maybe check for a configure
            // and prompt to confirm use of default config or provided config
        $builder->release();
        break;
    case "config": 
        $conf = getcwd().'/config/tlfbuild.json';
        if (!file_exists($conf)){
            echo Errors::config_file_doesnt_exist;
        }
        echo "\n\nFile config/tlfbuild.json:\n";
        echo file_get_contents($conf);
        echo "\n\n";
        break;
    case "--no-command--":
        echo "No command given. Run 'help'";
        break;
    default: 
        echo "Command '$command' is not available. Run 'help'";
        break;

}

echo "\n\n";
