<?php
/**
 * Use bump rules and sample commit messages to determine which index to bump on a version
 *
 */

require_once(dirname(__DIR__).'/src/VersionBumper.php');

use Tlf\Builder\VersionBumper;

// For X.Y.Z.G 
// X is based on branch
// Y is based on 'feature:' prefix
// Z is based on 'bugfix:' prefix
// G is based on 'other:' prefix
$bump_rules = [
    1 => [ 'feature' ],
    2 => [ 'bugfix' ],
    3 => [ 'other' ],
];

$tests = [
    'No Rules Match' => [
        'commit' => 'No prefix',
        'expect' => -1,
    ],
    'Index 1' => [
        'commit' => 'feature: tests',
        'expect' => 1,
    ],
    'Index 2' => [
        'commit' => 'bugfix: bugs',
        'expect' => 2,
    ],
    'Index 3' => [
        'commit' => 'other: documentation',
        'expect' => 3,
    ]
];

$pass_count = 0;
$fail_count = 0;

foreach ($tests as $test_name => $test_info){
    echo "\n ### Test $test_name ###\n";
    $commit_message = $test_info['commit'];
    $expect = $test_info['expect'];


    $index_to_bump = VersionBumper::get_index_to_bump($commit_message, $bump_rules);

    if ($index_to_bump == $expect){
        echo "    Pass: Commit '$commit_message' produced bump index '$index_to_bump', as expected.";
        $pass_count++;
    }
    else {
        echo "    Fail: Commit '$commit_message' produced bump index '$index_to_bump'. Expected '$expect'.";
        $fail_count++;
    }
}

echo "\n";
echo "\nTests Passed: $pass_count";
echo "\nTests Failed: $fail_count";
echo "\n\n";

