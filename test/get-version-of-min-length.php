<?php
/**
 * Ensure a version number is a minimum of a given length.
 *
 * @example 0.8 becomes 0.8.0.0 if the target length is 4
 *
 * The list of tags is expected to come from `git tag --list VERSION.*` 
 */

require_once(dirname(__DIR__).'/src/VersionBumper.php');

use Tlf\Builder\VersionBumper;


$tests = [
    // in each test name, X is the part of the version we are bumping
    'Same length'=>
    [
        'version' => '0.8',
        'length'=> 2, // 0-based index
        'expect'=>'0.8',
    ],
    'One Bigger'=>
    [
        'version' => '0.9',
        'length'=> 3, // 0-based index
        'expect'=>'0.9.0',
    ],
    'One Smaller (doesn\'t change it)'=>
    [
        'version' => '0.9',
        'length'=> 1, // 0-based index
        'expect'=>'0.9',
    ],
    'Empty version string'=>
    [
        'version' => '',
        'length'=> 1, // 0-based index
        'expect'=>'0',
    ],
    'One Digit Version' =>
    [
        'version' => '0',
        'length'=> 2, // 0-based index
        'expect'=>'0.0',
    ],


];

$pass_count = 0;
$fail_count = 0;

foreach ($tests as $test_name => $test_info){
    echo "\n ### Test $test_name ###\n";
    $version = $test_info['version'];
    $length  = $test_info['length'];
    $expect = $test_info['expect'];


    $minlength_version = VersionBumper::get_version_of_minlength($version, $length);

    if ($minlength_version == $expect){
        echo "    Pass: Input version '$version' with length '$length' becomes '$expect'";
        $pass_count++;
    }
    else {
        echo "    Fail: Input version '$version' with length '$length' became '$minlength_version'. Expected '$expect'";
        $fail_count++;
    }

}

echo "\n";
echo "\nTests Passed: $pass_count";
echo "\nTests Failed: $fail_count";
echo "\n\n";
