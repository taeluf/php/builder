<?php
/**
 * Determine the latest version number from a list of version strings.
 *
 * The list of tags is expected to come from `git tag --list VERSION.*` 
 */

require_once(dirname(__DIR__).'/src/VersionBumper.php');

use Tlf\Builder\VersionBumper;


$tests = [
    'Different version lengths'=>
    [
        'branch_version' => '0.8',
        'expect'=>'0.8.1.1',
        'tags'=> // essentially git tag --list 0.8.*
            "0.8.0
            0.8.1
            0.8.1.1",
    ],

    'Same version lengths'=>
    [
        'branch_version' => '0.8',
        'expect'=>'0.8.1.2',
        'tags'=> // essentially git tag --list 0.8.*
            "0.8.0.1
            0.8.1.1
            0.8.1.2",
    ],
    'Shorter version, bigger number'=>
    [
        'branch_version' => '0.8',
        'expect'=>'0.8.1',
        'tags'=> // essentially git tag --list 0.8.*
            "0.8.0.7
            0.8.1",
    ],

];

$pass_count = 0;
$fail_count = 0;

foreach ($tests as $test_num => $test_info){
    echo "\n ### Test $test_num ###\n";
    $branch_version = $test_info['branch_version'];
    $expect = $test_info['expect'];
    $git_tags = $test_info['tags'];

    $latest_version = VersionBumper::get_latest_version($git_tags, $branch_version);

    if ($latest_version == $expect){
        echo "    Pass: Expected version: $expect. Actual Version: $latest_version";
        $pass_count++;
    }
    else {
        echo "    Fail: Expected version: $expect. Actual Version: $latest_version";
        $fail_count++;
    }
}

echo "\n";
echo "\nTests Passed: $pass_count";
echo "\nTests Failed: $fail_count";
echo "\n\n";
