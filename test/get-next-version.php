<?php
/**
 * Determine the next version number from the current version number and a position to bump
 *
 * The list of tags is expected to come from `git tag --list VERSION.*` 
 */

require_once(dirname(__DIR__).'/src/VersionBumper.php');

use Tlf\Builder\VersionBumper;


$tests = [
    // in each test name, X is the part of the version we are bumping
    'Bump #.X'=>
    [
        'latest' => '0.8',
        'bump'=> 1, // 0-based index
        'expect'=>'0.9',
    ],

    'Bump X.#'=>
    [
        'latest' => '0.6',
        'bump' => 0,
        'expect'=>'1.0',
    ],
    'Bump #.X.#.#' =>
    [
        'latest' => '0.4.7',
        'bump' => 1,
        'expect' => '0.5.0',
    ],

    'Bump #.X to 4 digits' =>
    [
        'latest' => '0.4',
        'bump' => 3,
        'expect' => '0.4.0.1',
    ],

];

$pass_count = 0;
$fail_count = 0;

foreach ($tests as $test_name => $test_info){
    echo "\n ### Test $test_name ###\n";
    $latest = $test_info['latest'];
    $bump_index = $test_info['bump'];
    $expect = $test_info['expect'];


    $next_version = VersionBumper::get_bumped_version($latest, $bump_index);

    if ($next_version == $expect){
        echo "    Pass: Version '$latest' bumped at index '$bump_index' resulting in '$next_version'";
        $pass_count++;
    }
    else {
        echo "    Fail: Version '$latest' bumped at index '$bump_index', expecting '$expect', but getting '$next_version'";
        $fail_count++;
    }
}

echo "\n";
echo "\nTests Passed: $pass_count";
echo "\nTests Failed: $fail_count";
echo "\n\n";
