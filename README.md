# PHP Package Builder, Tagger, Releaser
An easy, configurable tool to build PHP projects into PHARs, setup CICD pipelines, and publish versioned tags.

**UNDER DEVELOPMENT** - Some documented features are not yet implemented

## Overview
1. `composer require taeluf/builder` or [Download a phar](https://gitlab.com/taeluf/php/builder/-/tags)
2. `php tlfbuild.phar create-config` (or create `config/tlfbuild.json`)
    - `tlfbuild.phar` is at path `vendor/taeluf/builder/bin/tlfbuild.phar`
3. Build phar: `php tlfbuild.phar build` to run your pipeline. 
4. Release: `php tlfbuild.phar release`

**Note:** `php tlfbuild.phar build-and-release` will build & releasee in a single step.

## Configure: `config/tlfbuild.json`
- `phar_name`: string name of the phar to create within the build directory
- `build_dir`: string relative path from project root to build dir, where build-related files will be created.
- `pipeline`: `object<string step_name, object settings>` pipeline steps and settings for each step
- `release`: Settings for how to bump the version number, depending upon commit messages. (*documentation below*)

```json
{
    "phar_name": "tlfbuild.phar",
    "build_dir": "build/",
    "pipeline": {
        "check_composer_json"=>{},
        "check_git_ignore"=>{},
        "create_composer_phar"=>{},
    },
    "release": {
        1: [ "feature", "bump" ],
        2: [ "bugfix", "fix", "bug" ],
        3: [ "*" ],
    }
}
```

*Note: When you run `release`, if the last commit starts with 'feature:', then 1.0.5.4 would be bumped to 1.1.0.0*

### Build Pipeline Steps Available
- `check_composer_json`: Check that composer.json exists and stop build if it does not. (*no settings*)
- `check_git_ignore`: Check if `.gitignore` lists `build/` & offer to add it to the file. (*no settings*)
- `create_composer_phar`: Create a phar using [clue/phar-composer](https://github.com/clue/phar-composer/).  (*no settings*)
- `create_phar`: Create a phar using built-in PharBuilder, which does not automatically build composer dependencies into the phar. (*configuration below*)

### pipeline: create_phar
Settings for `create_phar`. These options go under `config/tlfbuild.json -> pipeline: {"create_phar": {...} }`
- `bin`: string Relative path to php script to use as the CLI for your phar
- `dirs`: `array<string>` Relative path to directories to scan for php files to add to the phar
- `files`: `array<string>` Relative path to files to add to the phar
```json
    "create_phar":{
        "bin": "bin/tlfbuild.php",
        "dirs": ["src/"],
        "files": ["bin/bash-script"],
    }
```
**Tip:** Bash scripts in a phar cannot be read by the system, so you must `system(file_get_contents(__DIR__.'/path-to-file'))`

### release: Bumping version strings
Example:
```json
"release": {
    1: [ "feature", "bump" ],
    2: [ "bugfix", "fix", "bug" ],
    3: [ "*" ],
}
```

Based on the above settings
- A commit message starting with `feature:` or `bump:` will cause `1.0` to become `1.1`. 
- A commit message starting with `bugfix:`, `fix:`, or `bug:` will bump 1.0.0 to 1.0.1
- A commit message without one of the configured prefixes will bump 1.0.0.0 to 1.0.0.1

Settings Docs: 
- Type: `array<int index_in_version_string, array string_prefixes>` 
- string prefixes are literal, except asterisk (`*`).
- asterisk (`*`) will always bump that version number, and should only be present if you want a default case of bumping the last index.
- If existing version is `1.0`, and the 3rd index (1.0.0.X) is to be bumped, then the version string will be lengthened from `1.0` to `1.0.0.0` THEN bumped to `1.0.0.1`
