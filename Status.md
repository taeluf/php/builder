# Builder Status

## March 20, 2024
I think I'm basically done with the documentation. I need to still implement and/or test some of the documented features.

TODO:
- implement `release` cli command
- implement config/tlfbuild.json `release` configuration
- implement `tlfbuild.json -> pipeline -> create_phar: files` configuration (`dirs` is implemented already, `files` is not)
- test `create_composer_phar` build pipeline
- create `create-config` cli command
- create `build-and-release` cli command
- create an `--auto` cli option to disable prompts, for automated building & releasing
- Create a gitlab cicd yaml to automatically run the build & release steps 
    - and test it! Test on itself and on another project that's using the clue version
    - And add a cli command to create the gitlab cicd yaml file.
    - Add documentation about setting up the gitlab cicd 
- add this project to packagist
- Test the `vendor/bin/tlfbuild.phar` composer configuration
- Release this project (to versioned git tags)

TODO MAYBE:
- Add `--config rel/path/to/config.json` option, so that different pipelines can be run in different environments. 
- Add github cicd yaml

## March 13, 2024 LATER
I think I'm happy with the build pipeline stuff. This tool can at least build itself with no issues. 

Original plans from Feb 6 (now amended):
- DONE Package your PHP library into a PHAR 
    - MAY need to further test the clue implementation with other packages, since this one doesn't use it.
- Publish builds as git tags
    - publish-new-tag script will do this ... need to integrate into CLI
- DONE Auto-generate version numbers
    - AS IN this algo stuff is programmed. It's not integrated into release
- Setup CICD

I have an issue. I've built this to have a 'build' pipeline, but I have not considered a 'release' pipeline, which would require building in the process, UNLESS you force build & release to be separate steps.

You SHOULD be able to build only and use your own release process.

You SHOULD be able to release only and use your own build process.

You SHOULD be able to build & release as a single process.

Currently, release works by creating a new branch, generating the phar, then adding it to the bin dir on the build branch, then committing the build branch (but only locally!) then pushing it to a git tag, then switching back to original branch & deleting the build branch.

Also, the release code is all written in bash. In theory, I should make everything PHP. In practice, it doesn't matter ... UNLESS this project gets some attention / traction AND people request a bash-free implementation AND somebody contributes a the code for that. 

(thought about it) - If I keep it as bash, then I have to pipe the entire file content to system() instead of passing the file path to system(), bc PHP can read "files" within phars, but the system cannot, bc the phar is an archive, not a directory.

DONE:
- add 'release' command with some notes (no implementation)
- add 'config' command to print current config (or err if not exist)
- add 'release' and 'config' to help menu
- rename 'run-tests' to 'self-test'
- other cli cleanup? I think so
- add config_file_missing error to Errors class

## March 13, 2024
improve create_phar pipeline to add ALL php files in configured directories, defaulting with just src/. And allow configuration of bin script, build directory, and out

Added some documentation on Overview, configuration, and the create_phar pipeline.

Created `src/Errors.php` to hold static error messages.

TODO:
- See March 4 notes
- DOCUMENT AS YOU GO


## March 4, 2024
I moved the build-self command into the build command as a pipeline and added support for config/tlfbuild.json to define your own pipeline. pipelines now are passed options as an array. The self-build seems to be working wonderfully! I tried to make the phar file executable, but was unsuccessful. I think i'll give up on that and require it just be run via `php whatever.phar`.

TODO:
- DONE clean up the cli (remove the self build command)
- DONE document the build configuration 
- DUMB continue to clean up
- DONE provide more options for built-in phar-builder 
    - DONE let it scan the vendor dir
    - DONE configurable stub (bin script)

## March 2, 2024 final notes
DONE: 
- moved `bin/tlfbuild` to `bin/tlfbuild.php` because I couldn't get the stub to work without the filename 
- Modfiied `bin/tlfbuild.php` to use `file_get_contents(bin/build-phar)` and pass that file content to `system()` instead of passing the file path `bin/build-phar` to `system()`. This is because `sh` does not seem to be able to load a file that is INSIDE a phar archive.
- Created `tlfbuild.php test-build-phar` command to test the custom phar builder that builds this project.
    - TODO: This seems pretty easy, and I might want to replace phar-composer. Basically, I'd loop over packages in the vendor/composer/installed.json and add each project directory. Might need to modify some excludes or something. Would be nice to leave clue/phar-composer as an option for builds
- I tested the `build` script on my library LilDb, and it appeared to work. LilDb's internal `help` command worked as expected, when calling the lildb phar. The only other command it supports is migrate, and there's setup for that.

TODO:
- Test this with code scrawl and phptester. see if I can run tests for the pwd() project and if i can generate documentation for the pwd() project. 
- Clean it up?
- Test replacing phar-composer with my own custom pharbuilder.

## March 2, 2024
I've craeted `src/PharBiulder.php` and moved bin script to `bin/tlfbuild.php`, bc I couldn't get stub to work without filename. I'm adding php files no problem, and I think I'm successfully adding my bash script as well. However... My build script calls upon the bash script `bin/build-phar`. `bin/build-phar` is executed with the `system()` command. So the `system()` command is using `sh`, and is unable to resolve a `phar:///...` path to an actual file. So it tries to run `phar:///home/.../test.phar/bin/build-phar`. It can find `test.phar` itself (I'm pretty sure), but it cannot find a file WITHIN the test.phar. I don't know how to fix this, except for converting to PHP, which I should probably do anyway.

The phar I'm generating with `src/PharBuilder.php` can successfully run tests! So that's good. And the output phar DOES actually contain the file content of the bash script. It just isn't accessible by the system.

SOLVED: In tlfbuild script, I converted the path-based system() command call to a `system(file_get_contents(bin/build-phar))` ... the php environment can resolve paths inside of a phar, so I'm passing a string to the system() rather than a file path

## Feb 14, 2024
I cleaned up the `bin/build-phar` script a bit, fixed some pathing (but output phar is just test.phar). Then tried running the build via the generated phar, and the bin script `bin/build-phar` does not exist.

phar-composer is NOT adding `bin/build-phar` to the output `.phar` file.

It may be best to convert that script to PHP. But Also, i don't really care, so if I can get the bash to work I want to.

Also, I want this project to have it's own phar-builder for building itself. I don't really think I want to replace phar-composer with my own custom implementation. But I do want this project to be able to build itself, probably. 

## Feb 13, 2024
- VersionBumper is fully tested and implemented
- `bin/build-phar` is under development:
    - done: copied from code scrawl
    - todo: update paths / naming (currently uses code-scrawl.phar explicitly)
    - todo: Make it work! (clue/phar-composer requires me to run composer install. I DO NOT WANT TO RELY ON COMPOSER, so i might make a custom phar builder just for this project)
- src/ProjetBuilder.php
    - done: created simple build pipeline
    - todo: allow automated execution of create_phar (requires passing some paramaters to build-phar script)
    - todo: Finish create_phar build step
    - todo: maybe make custom PHAR builder just for this project
    - todo: MAYBE add hash verification of downloaded phar-composer.phar

## Feb 8, 2024
I moved some code from GitVersionBumper into VersionBumper. see static public function get_version_of_minlength(string $version, int $min_length).

I didn't get it working. I made a test file for get-ersion-f-min-llength. it's probalby not finished.

work got interrupted, so yeah, it's in a messy state.

## Feb 6, 2024 (notes from work later)
- 2 new test files
- Refactored VersionBumper and created GitVersionBumper
- See GitVersionBumper ... there is a TODO to move a block of code to VersionBumper and create a test for it.
- Started marking items as 'done' below

## Feb 6, 2024 (notes from work)
- `src/VersionBumper.php` copy+pasted from scrawl. Implemented `static public function get_latest_version(string $versions_list, string $version_prefix=''): string`
- `test/determine-latest-version.php` will tests `get_latest_version()`
- `bin/tlfbuild` has a rudimentary CLI setup that runs all `.php` files in the `test/` directory
- Took detailed notes/plans below
- Wrote SOME README documentation

Next Steps:
This project will remain incredibly simple and dependency free other than clue/phar-composer. More functionality needs built out & tested. It is all detailed below. Some of it is already impemented in Code Scrawl & will need copied into this repo. But those prototype implementations will likely need some changes. Idk. We'll see. And I'll need a composer.json so that it can be distributed through packageist (even though it will not USE packagist for its own dependency).

## Feb 6, 2024 (plans)
What features does this project offer to PHP Developers?
- Package your PHP library into a PHAR
- Publish builds as git tags
- Auto-generate version numbers
- Setup CICD

What does it need in order to accomplish this?
- Documentation
    - installation
    - running the cli
    - setting up cicd
    - configure
- CLI
    - help
    - build-and-release
    - create-phar
    - DONE run-tests
    - create-build-branch
    - push-new-version
- Package PHAR
    - Download clue/phar-composer.phar
    - Run clue/phar-composer.phar
- Create git tags that include the PHAR 
    - Create a build branch (local)
    - Package Phar
    - DONE Determine version
    - commit build branch
    - push to git tags
    - cleanup build branch
- Auto-generate version numbers
    - DONE Determine latest published build version for current branch
    - DONE Read & apply bump rules (returning a new version number)
- Setup CICD
    - gitlab cicd template
- Testing
    - Any algorithmic pieces - mainly just determining the latest version number from the list of git tags, and generating the next version number from the latest version + the bump rules

Nice To Haves:
- Configure file excludes for the output phar 
- Configure WHERE the phar is output to
- Manage the clue/phar-composer.phar version to ensure consistent builds
- github cicd template
- Ability to add different build steps, and the phar-composer build step become a modular step rather than a core feature. This is just because I anticipate wanting additional build steps in the future (such as building my documentation!)
